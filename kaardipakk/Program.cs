﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kaardipakk
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            List<string> pakk = new List<string>();//nimetustega pakk
            List<int> kaardihulk = new List<int>();
            kaardihulk = Enumerable.Range(1, 52).ToList(); //Loob 52 kaarti

            void loog(string midagiTeen)
            {
                Console.Write($"{midagiTeen}");
            }

            void log(string teenMidagi)
            {
                Console.WriteLine(teenMidagi);
            }

            void kaardipakiLoomine()
            {
                for (int järk = 0; järk < 52;)
                {
                    int nr = järk;
                    void väärtusteJagamine()
                    {
                        void jaotamine()
                        {
                            if (0 == nr)
                            {
                                pakk.Add("A");
                            }
                            else if (0 == nr % 12)
                            {
                                pakk.Add("K");
                            }
                            else if (0 == nr % 11)
                            {
                                pakk.Add("Q");
                            }
                            else if (0 == nr % 10)
                            {
                                pakk.Add("J");
                            }
                            else if (0 == nr % 9)
                            {
                                pakk.Add("10");
                            }
                            else if (0 == nr % 8)
                            {
                                pakk.Add("9");
                            }
                            else if (0 == nr % 7)
                            {
                                pakk.Add("8");
                            }
                            else if (0 == nr % 6)
                            {
                                pakk.Add("7");
                            }
                            else if (0 == nr % 5)
                            {
                                pakk.Add("6");
                            }
                            else if (0 == nr % 4)
                            {
                                pakk.Add("5");
                            }
                            else if (0 == nr % 3)
                            {
                                pakk.Add("4");
                            }
                            else if (0 == nr % 2)
                            {
                                pakk.Add("3");
                            }
                            else if (0 == nr % 1)
                            {
                                pakk.Add("2");
                            }
                            else
                            {
                            }
                        }

                        jaotamine();
                    }


                    #region mastideJagamine
                    if (järk < 13)
                    {
                        väärtusteJagamine();
                        pakk[järk] += "♥";
                    }
                    else if (järk > 12 && järk < 26)
                    {
                        nr -= 13;
                        väärtusteJagamine();
                        pakk[järk] += "♦";
                    }
                    else if (järk > 25 && järk < 39)
                    {
                        nr -= 26;
                        väärtusteJagamine();
                        pakk[järk] += "♠";
                    }
                    else
                    {
                        nr -= 39;
                        väärtusteJagamine();
                        pakk[järk] += "♣";
                    }
                    #endregion
                    järk++;
                }
            }

            void trykiPakk()
            {
                for (int järk = 0; järk < 52; järk++)
                {
                    int nr = järk + 1;
                    if (nr % 13 == 0)
                    {
                        log($"{pakk[järk]}");
                    }
                    else
                    {
                        loog($"{pakk[järk]}");
                    }
                }
            }

            kaardipakiLoomine();
            trykiPakk();
            log("Kaardipakk loodud!");
            List<string> segatudPakk = pakk.OrderBy(a => Guid.NewGuid()).ToList();
            for (int järk = 0; järk < 52; järk++)
            {
                int nr = järk + 1;
                if (nr % 13 == 0)
                {
                    log($"{segatudPakk[järk]}");
                }
                else
                {
                    loog($"{segatudPakk[järk]}");
                }
            }

        }
    }
}
